/* eslint-disable @typescript-eslint/no-unused-vars */

/**
 * El código que proporcionaste es un módulo de JavaScript/TypeScript escrito para una aplicación de React que maneja temas y  
 * estilos utilizando la librería Material-UI (también conocida como MUI). Vamos a desglosar el código línea por línea:
 * 1. `import { FC, useState, createContext, useEffect } from 'react';`: Estas son importaciones de módulos de React que se utilizan en el código.
 *  - `FC`: Un alias para `FunctionComponent` que se usa para definir componentes funcionales.
 * - `useState`: Un hook de React que permite gestionar el estado en componentes funcionales.
 *  - `createContext`: Una función que se utiliza para crear un contexto en React.
 *  - `useEffect`: Un hook de React que permite ejecutar efectos secundarios en componentes funcionales.
 */
import React, { createContext } from 'react';
import { createTheme } from '@mui/material/styles';
// nos ayuda a proporcionar un tema personalizado
import { ThemeProvider } from '@mui/material';
// utilizado para crar un tema personalizado 
// import { themeCreator } from './base';
// gestiona el orden en que se inyectan los estilos en la aplicación.
import { StylesProvider } from '@mui/styles';
import { themeCreator } from './base';

// crea un contexto llamado `ThemeContext` se utiliza para compartir y administrar información sobre el tema en toda la aplicación, por defecto no se le asigna nada
export const ThemeContext = createContext((_themeName: string): void => { });

// component function
const themeName = createTheme(themeCreator('NebulaFighterTheme'))

function ThemeProviderWrapper({ children }: { children: React.ReactNode }) {

  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={themeName}>{children}</ThemeProvider>
    </StylesProvider>
  );
}

export default ThemeProviderWrapper
