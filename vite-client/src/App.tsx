
import {
  //   Typography,
  Box,
  //   Card,
  //   Container,
  //   Button,
  styled,
} from '@mui/material';
// import Logo from './components/Logo'
// import Hero from './content/Hero'


import Sidebar from './layouts/SidebarLayout'

import { Outlet } from "react-router-dom";
// const HeaderWrapper = styled(Card)(
//   ({ theme }) => `
//   width: 100%;
//   display: flex;
//   align-items: center;
//   height: ${theme.spacing(10)};
//   margin-bottom: ${theme.spacing(10)};
// `
// );

const OverviewWrapper = styled(Box)(
  ({ theme }) => `
    overflow: auto;
    background: ${theme.palette.common.white};
    flex: 1;
    overflow-x: hidden;
`
);

function App() {

  return (
    <>
      <OverviewWrapper sx={{
        height: '100vh',
      }}>
        < Sidebar >
          <Outlet />
          {/* <DashboardTasks /> */}
        </Sidebar >
      </OverviewWrapper >
    </>
  )
}

export default App
