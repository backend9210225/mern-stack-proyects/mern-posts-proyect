import React from 'react'
import ReactDOM from 'react-dom/client'
import ThemeProvider from './theme/ThemeProvider.tsx'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

import App from './App'
import Status404 from './404.tsx'

import DashboardTasks from './Pages/dashboards/tasks/index'
import ApplicationsMessenger from './Pages/applications/messenger'
import ApplicationsTransactions from './Pages/management/transactions'
import ManagementUserProfile from './Pages/management/profile'
import ManagementUserSettings from './Pages/management/profile/settings'
const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <Status404 />,
    children: [
      {
        index: true,
        element: <DashboardTasks />
      },
      {
        path: '/dashboards/tasks',
        element: <DashboardTasks />
      },
      {
        path: '/applications/messenger',
        element: <ApplicationsMessenger />
      },
      {
        path: '/management/transactions',
        element: <ApplicationsTransactions />
      },
      {
        path: '/management/profile',
        element: <ManagementUserProfile />
      },
      {
        path: '/management/profile/settings',
        element: <ManagementUserSettings />
      }
    ]
  }
])


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>

    <ThemeProvider>
      <RouterProvider router={router} />
      {/* <App /> */}
    </ThemeProvider>

  </React.StrictMode>,
)
