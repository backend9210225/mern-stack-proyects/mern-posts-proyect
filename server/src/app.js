import express from "express";
import cors from "cors";
import morgan from "morgan";
import cookieParser from "cookie-parser";
import { userRoute } from "./routes/user.routes.js";
import { postRoute } from "./routes/post.routes.js";
export const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan("dev"));
app.use(cookieParser());
app.use("/api", userRoute);
app.use("/api", postRoute);
