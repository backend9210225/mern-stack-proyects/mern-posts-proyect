import { config } from "../config/configurations.js";
import jwt from "jsonwebtoken";
const TOKEN_SECRET = config.jwt_key
export function createAccessToken(payload) {
  return new Promise((resolve, reject) => {
    // creamos una nueva promesa que ejecuta una funcion que si todo esta corrrecto es resolve si hay error es reject
    jwt.sign(
      // crea el toquen
      payload,
      TOKEN_SECRET,
      {
        expiresIn: "1d", //define por cuanto tiempo
      },
      (err, token) => {
        // lo volvemos aincrono
        if (err) reject(err);
        resolve(token);
      }
    );
  });
}
