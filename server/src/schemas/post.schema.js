import { z } from "zod";

export const registerPostSchema = z.object({
  title: z
    .string({
      required_error: "Title is required",
    })
    .min(6, { message: "Must be 6 or more characters long" })
    .max(20, { message: "Must be 30 or less characters long" }),
  body: z
    .string({
      required_error: "Body is required",
    })
    .min(15, { message: "Must be 6 or more characters long" })
    .max(50, { message: "Must be 30 or less characters long" }),
});

export const updatePostSchema = z.object({
  title: z
    .string()
    .min(6, { message: "Must be 6 or more characters long" })
    .max(20, { message: "Must be 30 or less characters long" }),
  body: z
    .string()
    .min(15, { message: "Must be 6 or more characters long" })
    .max(50, { message: "Must be 30 or less characters long" }),
});
