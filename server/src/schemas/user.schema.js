import { z } from "zod";

export const registerUserSchema = z.object({
  userName: z
    .string({
      required_error: "Username is required",
    })
    .min(4, { message: "Must be 6 or more characters long" }),
  email: z
    .string({
      required_error: "Email is required",
    })
    .email({ message: "Invalid email" }),
  password: z
    .string({
      required_error: "password is required",
    })
    .min(6, {
      message: "password must be at least 6 characters long",
    }),
});

export const loginUserSchema = z.object({
  email: z
    .string({
      required_error: "Email is required",
    })
    .email({
      message: "invalid email address",
    }),
  password: z
    .string({
      required_error: "password is required",
    })
    .min(6, {
      message: "password must be at least 6 characters long",
    }),
});
