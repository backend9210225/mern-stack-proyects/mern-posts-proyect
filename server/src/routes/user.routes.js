import { Router } from "express";
import { validateSchema } from "../middleware/validateSchemas.js";
import { loginUserSchema, registerUserSchema } from "../schemas/user.schema.js";
import { register, login, logout } from "../controllers/user.controller.js";
export const userRoute = Router();

userRoute.post("/register", validateSchema(registerUserSchema), register);
userRoute.post("/login", validateSchema(loginUserSchema), login);
userRoute.post("/logout", logout);
