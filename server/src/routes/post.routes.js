import { Router } from "express";
import {
  createPost,
  deletePost,
  getAllPosts,
  getPostById,
  getAllPostsOfUser,
  updatePost
} from "../controllers/post.controller.js";

import { authRequire } from "../middleware/validatejwt.js";

import {
  registerPostSchema,
  updatePostSchema,
} from "../schemas/post.schema.js";

import { validateSchema } from "../middleware/validateSchemas.js";

export const postRoute = Router();

postRoute.post(
  "/create",
  authRequire,
  validateSchema(registerPostSchema),
  createPost
);

postRoute.get("/posts", getAllPosts);
postRoute.get("/posts/:id", authRequire, getPostById);
postRoute.patch("/post/:id", authRequire, updatePost);
postRoute.get("/user", authRequire, getAllPostsOfUser);
postRoute.delete("/post/:id", authRequire, deletePost);
