import mongoose from "mongoose";
import { config } from "../config/configurations.js";
const uri = config.uri;
export const connector = async () => {
  try {
    await mongoose.connect(uri);
    console.log("Connected to MongoDB");
  } catch (error) {
    console.log("Error connecting to MongoDB", error);
  }
};
