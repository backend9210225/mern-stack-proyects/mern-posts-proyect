import { app } from "./app.js";
import { config } from "./config/configurations.js";
import { connector } from "./DB/DatabaseConnector.js";
const port = config.port;
console.log(port);
connector();
app.listen(port, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`Server is running on port ${port}`);
  }
});
