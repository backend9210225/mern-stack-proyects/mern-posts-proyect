import User from "../DB/models/user.model.js";
import bcrypt from "bcryptjs";
import { createAccessToken } from "../libs/jwt.js";

export const register = async (req, res) => {
  const { userName, email, password } = req.body;
  try {
    const passwordHassh = await bcrypt.hash(password, 10);
    const user = new User({
      userName,
      email,
      password: passwordHassh,
    });
    const userFound = await user.save();
    const usetToken = await createAccessToken({
      id: userFound.id,
      user: userFound.userName,
    });
    res.cookie("token", usetToken);
    res.status(201).json({ message: "user created successfully" });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(500).json({ message: "email already exists" });
    } else {
      return res.status(500).json({ message: error.message });
    }
  }
};

export const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const userFound = await User.findOne({ email });
    if (!userFound) return res.status(404).json({ message: "user not found" });

    const isMath = await bcrypt.compare(password, userFound.password);

    if (!isMath)
      return res.status(400).json({ message: "password is incorrect" });
    const usetToken = await createAccessToken({
      id: userFound.id,
      user: userFound.userName,
    });
    res.cookie("token", usetToken);
    res.status(201).json({ message: "login is successful" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const logout = (req, res) => {
  res.clearCookie("token");
  res.status(200).json({ message: "logout is successful" });
};
