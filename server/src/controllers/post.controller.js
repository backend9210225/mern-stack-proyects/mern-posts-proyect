import Post from "../DB/models/post.model.js";
export const createPost = async (req, res) => {
  const { title, body } = req.body;
  const post = new Post({
    title,
    body,
    postManager: req.user.id,
  });
  const postFound = await post.save();
  res.status(201).json(postFound);
};

export const getAllPosts = async (req, res) => {
  try {
    const posts = await Post.find().populate("postManager", "userName");
    res.status(200).json(posts);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getAllPostsOfUser = async (req, res) => {
  try {
    console.log(req.user);
    const posts = await Post.find({
      postManager: req.user.id,
    }).populate("postManager", "userName");
    res.status(200).json(posts);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getPostById = async (req, res) => {
  try {
    const { id } = req.params;
    const post = await Post.findById(id);
    if (!post) return res.status(404).json({ message: "Post not found" });
    res.status(200).json(post);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updatePost = async (req, res) => {
  const task = await Post.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  if (!task) return res.status(404).json({ message: "Post not found" });

  res.status(200).json({ message: "Post updated", task });
};

export const deletePost = async (req, res) => {
  try {
    const { id } = req.params;
    const post = await Post.findByIdAndDelete(id);
    if (!post) return res.status(404).json({ message: "Post not found" });

    res.status(200).json(post);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
