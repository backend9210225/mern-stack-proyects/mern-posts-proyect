import jwt from "jsonwebtoken";
import { config } from "../config/configurations.js";

const TOKEN_SECRET = config.jwt_key;
export const authRequire = (req, res, next) => {
  const { token } = req.cookies;

  if (!token)
    return res.status(401).json({
      message: "Unauthorized",
    });
  jwt.verify(token, TOKEN_SECRET, (err, user) => {
    if (err) return res.status(403).json({ message: "Forbidden" });
    req.user = user;
    next();
  });
};
